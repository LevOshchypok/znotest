/* eslint-disable react/jsx-no-undef */
import './App.css';
import { Menu } from './app/menu/menu';
import QuestionForm from './app/question/question-form'
import QuestionList from './app/question/question-list/question-list';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

// routers 
// notification service 
// general loader 

const router = createBrowserRouter([
  {
    path: "/",
    element: <div>
    <QuestionForm />
    <QuestionList topicRef={1}/>
  </div>,
  },
  {
    path: "/test",
    element: <div>
    <h1>Test</h1>
  </div>,
  }
]);


function App() {
  return (
    <div>
      <div className="custom-app-root">
       <Menu />
       <RouterProvider router={router} />
      </div>
    </div>
  );
}

export default App;
