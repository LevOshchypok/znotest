import { Card, CardContent } from "@mui/material"
import { Component } from "react"
import { Question } from "../question-models"
import QuestionService from "../question-service"
import './question_list.css'


type MyState = {
    questions: Question[]
}

type MyProps = {
    topicRef: number
}

class QuestionList extends Component<MyProps, MyState>{

    state: MyState = {
        questions: []
    }

    getQuestions = (topicRef: number) => {
        QuestionService.getQuestions(topicRef)
        .then(res => {
            console.log("res with question", res.data)
            this.setState({questions: res.data.data})
        })
    }

    componentDidMount(): void {
        this.getQuestions(this.props.topicRef)
    }

    render() {
        const {topicRef} = this.props
        return (<div className="question-list-root">
            <h1>Питання</h1>
            <div className="question-list-items">
                {this.state.questions.map(q => <QuestionItem key={q.id}  question={q}/>)}
            </div>
        </div>)
    }

}

export default QuestionList

interface QuestionItem {
    question: Question
}

const QuestionItem = ({question}: QuestionItem) => {

    return (<Card>
        <CardContent>
        <h3>{question.text}</h3>
        <ol>
            {question.answers.map(ans =>
                <li>{ans.text}</li>)}
        </ol>
        </CardContent>
    </Card>)
}