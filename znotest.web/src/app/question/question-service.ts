import { request } from "../../common/client";
import { Question } from "./question-models";

export default class QuestionService {

    static postQuestion(question: Question){
        return request("/api/Question", {method: 'POST', payload: question})
    }

    static getQuestions(topicRef: number){
        return request(`/api/Question/topic/${topicRef}`, {method: 'GET'})
    }

}