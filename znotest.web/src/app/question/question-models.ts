export interface Answer {
    id?: number, 
    text: string, 
    isCorrect: boolean, 
    orderNum?: number
}

export interface Question {
    id?: number, 
    text?: string, 
    topicRef?: number,
    answers: Answer[]
}
