import { useState, useEffect } from "react";
import { CTText, CTCheckBox } from "../../common/general-inputs";
import './answer-form.css'

export const AnswerForm = ({ orderNum, onChange, forceCleanup }: any) => {
    const [text, setText] = useState('');
    const [isCorrect, setIsCorrect] = useState(false);

    useEffect(() => {
        onChange({ text, isCorrect });
        if (forceCleanup) {
            setText('')
            setIsCorrect(false)
        }
    }, [text, isCorrect, forceCleanup])

    const handleTextChange = (e: React.ChangeEvent<HTMLInputElement>) => { setText(e.target.value) };
    const handleIsCorrectChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        let newIsCorrect = !isCorrect;
        setIsCorrect(newIsCorrect)
    };

    return (
        <form className="answer-form">
            <span>{orderNum}</span>
            <CTText
                name="text"
                label="Варіант відповіді"
                value={text}
                className="answer-text"
                onChange={handleTextChange}
            />
            <CTCheckBox
                name="isCorrect"
                label="Чи правильне?"
                value={isCorrect}
                onChange={handleIsCorrectChange}
            />
        </form>
    )
}