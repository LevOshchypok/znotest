import { useState } from 'react';
import './menu.css'

// щось не дуже працює, але нехай буде)) 


export const Menu = () => {
    const [isMenuOpen, setIsMenuOpen] = useState(true);

    const toggleMenu = () => {
        setIsMenuOpen(!isMenuOpen);
    };

    return (
        <div>
            <button onClick={toggleMenu}>Toggle Menu</button>
            <nav
            style={{
            width: isMenuOpen ? '200px' : '0px',
            transition: 'width 0.5s ease',
            }}
              >
            <ul>
            <li>Menu Item 1</li>
            <li>Menu Item 2</li>
            <li>Menu Item 3</li>
            </ul>
            
        </nav>
      </div>
    )
}