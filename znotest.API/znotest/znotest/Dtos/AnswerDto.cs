﻿namespace znotest.Dtos
{
    public class AnswerDto
    {
        public int Id { get; set; }
        public string? Text { get; set; }
        public int OrderNum { get; set; }
        public bool IsCorrect { get; set; }
    }
}
