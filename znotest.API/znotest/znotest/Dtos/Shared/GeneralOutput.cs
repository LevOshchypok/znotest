﻿namespace znotest.Dtos.Shared
{
    public class GeneralOutput<T>
    {
        public Pager paging { get; set; }
        public IEnumerable<T> data { get; set; }
    }
}
