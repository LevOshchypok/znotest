﻿namespace znotest.Dtos.Shared
{
    public class Pager
    {
        public int totalPage { get; set; }
        public int totalEntries { get; set; }
        public int pageNum { get; set; }
        public int pageSize { get; set; }

    }

}
