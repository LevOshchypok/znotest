﻿using Microsoft.EntityFrameworkCore;
using znotest.Dtos.Shared;

namespace znotest.Helpers
{
    public static class QueryableExtention
    {
        public static async Task<GeneralOutput<TSource>> ToPagingTokenAsync<TSource>(this IQueryable<TSource> target,
            int? page_num, int? page_size)
        {
            var pageSize = page_size ?? 25;
            var pageNum = page_num ?? 1;
            var skip = (pageNum - 1) * pageSize;
            var query = target.Skip(skip).Take(pageSize);
            var data = await query.ToArrayAsync();

            var total = await target.CountAsync();

            var paging = new Pager()
            {
                pageNum = pageNum,
                pageSize = pageSize,
                totalEntries = total,
                totalPage = (int)Math.Ceiling(total * 1.00 / pageSize)
            };

            var result = new GeneralOutput<TSource>
            {
                data = data,
                paging = paging
            };

            return result;
        }
    }
}
