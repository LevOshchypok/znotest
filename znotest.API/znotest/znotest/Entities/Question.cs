﻿namespace znotest.Entities
{
    public class Question
    {
        public int Id { get; set; }
        public string? Text { get; set; }
        public int TopicRef { get; set; }
        public string? InsertedBy { get; set; }
        public DateTime InsertedAt { get; set; }
        public virtual ICollection<Answer> Answers{ get; set; }    
    }
}
