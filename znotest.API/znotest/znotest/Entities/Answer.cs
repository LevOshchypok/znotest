﻿namespace znotest.Entities
{
    public class Answer
    {
        public int Id { get; set; }
        public string? Text { get; set; }
        public int OrderNum { get; set; }
        public bool IsCorrect { get; set; }
        public int QuestionRef { get; set; }
        public virtual Question Question { get; set; }
        public string? InsertedBy { get; set; }
        public DateTime InsertedAt { get; set; }
    }
}
