﻿namespace znotest.Entities
{
    public class Topic
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? InsertedBy { get; set; }
        public DateTime InsertedAt { get; set; }

    }
}
