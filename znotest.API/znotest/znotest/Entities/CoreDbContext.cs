﻿using Microsoft.EntityFrameworkCore;

namespace znotest.Entities
{
    public class CoreDbContext: DbContext
    {
        public CoreDbContext(DbContextOptions<CoreDbContext> options) : base(options)
        {

        }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Topic>().ToTable("Topic");
            modelBuilder.Entity<Question>(entity =>
            {
                entity.ToTable("Question");

                entity
                .HasMany(q => q.Answers)
                .WithOne(a => a.Question)
                .HasForeignKey(a => a.QuestionRef)
                .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<Answer>(entity =>
            {
                entity.ToTable("Answer");
            });
        }

    }
}
