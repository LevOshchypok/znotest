﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using znotest.Dtos;
using znotest.Repositories;

namespace znotest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionController : ControllerBase
    {
        private readonly IQuestionRepository _repo;

        public QuestionController(IQuestionRepository repo)
        {
            _repo = repo;
        }

        [HttpGet("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(QuestionDto))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<QuestionDto>> getQuestionDetail([FromRoute] int id)
        {
            var question = await _repo.GetQuestion(id);
            if (question != null)
            {
                return Ok(question);
            }

            return NotFound();
        }

        [HttpGet("topic/{topicId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<QuestionDto>))]
        public async Task<ActionResult> getQuestionsByTopic([FromRoute] int topicId)
        {
            return Ok(await _repo.GetQuestionList(topicId));
        }

        [HttpPost]
        public async Task<ActionResult<int>> createQuestion([FromBody] QuestionDto dto)
        {
            var id = await _repo.CreateQuestion(dto);
            return id;
        }

        [HttpPut]
        public async Task<ActionResult<bool>> updateQuestion([FromBody] QuestionDto dto)
        {
            return Ok(await _repo.UpdateQuestion(dto));
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> deleteQuestion([FromRoute] int id)
        {
            await _repo.DeleteQuestion(id);
            return Ok();
        }
        
    }
}
