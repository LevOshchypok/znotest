﻿using Microsoft.AspNetCore.Mvc;
using znotest.Dtos;
using znotest.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace znotest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TopicController : ControllerBase
    {
        private readonly ITopicRepository _repo;

        public TopicController(ITopicRepository repo)
        {
            _repo = repo;
        }


        // GET: api/<TopicController>
        [HttpGet(Name = "GetTopics")]
        [Produces(typeof(TopicDto))]
        public async Task<IEnumerable<TopicDto>> GetTopics()
        {
            return await _repo.GetTopics();
        }

        //// GET api/<TopicController>/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<TopicController>
        [HttpPost]
        public async Task<int> createTopic([FromBody] TopicDto dto)
        {
            return await _repo.CreateTopic(dto);
        }

        // PUT api/<TopicController>/5
        [HttpPut("{id}")]
        public async Task<bool> updateTopic([FromBody] TopicDto dto)
        {
            return await _repo.UpdateTopic(dto);
        }

        // DELETE api/<TopicController>/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _repo.DeleteTopic(id);
        }
    }
}
