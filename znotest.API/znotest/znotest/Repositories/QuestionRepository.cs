﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using znotest.Dtos;
using znotest.Dtos.Shared;
using znotest.Entities;
using znotest.Helpers;

namespace znotest.Repositories
{
    public class QuestionRepository : IQuestionRepository
    {
        private readonly CoreDbContext _context;   
        private readonly IConfiguration _configuration;
        private readonly ILogger<QuestionRepository> _logger;

        public QuestionRepository(IConfiguration configuration, 
            ILogger<QuestionRepository> logger,
            CoreDbContext context)
        {
            _configuration = configuration;
            _logger = logger;
            _context = context;
        }

        public async Task<int> CreateQuestion(QuestionDto dto)
        {
            using (var conn = new SqlConnection(_configuration.GetValue<string>("ConnectionStrings:DefaultConnection")))
            {
                conn.Open();

                string sql = "insert Question (text, topicRef, insertedAt, insertedBy)";
                sql += "select @text, @topicRef, getdate(), 'auto' ";
                sql += "select cast(scope_identity() as int) ";

                string sqlAnswer = "insert Answer (text, orderNum, isCorrect, questionRef, insertedBy, insertedAt)";
                sqlAnswer += "values (@text, @orderNum, @isCorrect, @questionRef, 'auto', getdate())";

                var transaction =  await conn.BeginTransactionAsync();
                int questionId = 0;

                try
                {
                    questionId = await conn.QuerySingleAsync<int>(sql, new { text = dto.Text, topicRef = dto.TopicRef }, transaction);

                    List<object> answers = new List<object>(dto.Answers.Count());
                    foreach (var ans in dto.Answers)
                    {
                        answers.Add(new { text = ans.Text, orderNum = ans.OrderNum, isCorrect = ans.IsCorrect, questionRef = questionId });
                    }

                    int affectedRows = await conn.ExecuteAsync(sqlAnswer, answers, transaction);

                    transaction.Commit();
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    throw;
                }
                
                return questionId;
            }
        }

        public async Task DeleteQuestion(int id)
        {
            var question = await _context.Questions.Where(q => q.Id == id).FirstOrDefaultAsync();

            if (question != null)
            {
                var answers = await _context.Answers.Where(a => a.QuestionRef == id).ToListAsync();

                _context.RemoveRange(answers);
                _context.Remove(question);
                await _context.SaveChangesAsync();
            }
            
        }

        public async Task<QuestionDto> GetQuestion(int id)
        {
            return await _context.Questions
                .Where(q => q.Id == id)
                .Select(q => new QuestionDto()
                {
                    Id = q.Id,
                    Text = q.Text,
                    Answers = q.Answers.Select(a => new AnswerDto
                    {
                        Id = a.Id,
                        IsCorrect = a.IsCorrect,
                        OrderNum = a.OrderNum,
                        Text = a.Text
                    })
                }).SingleOrDefaultAsync();
        }

        public async Task<GeneralOutput<QuestionDto>> GetQuestionList(int topicRef)
        {
           return await _context.Questions
                .Select(q => new QuestionDto() { 
                    Id = q.Id, 
                    Text = q.Text, 
                    Answers = q.Answers.Select(a => new AnswerDto {
                    Id = a.Id,
                    IsCorrect = a.IsCorrect,
                    OrderNum = a.OrderNum,
                    Text = a.Text
                    })
                })
                .ToPagingTokenAsync(null, null);
        }

        public async Task<bool> UpdateQuestion(QuestionDto dto)
        {
            var question = await _context.Questions.Where(q => q.Id == dto.Id).FirstOrDefaultAsync();
            var existedAnswers = await _context.Answers.Where(a => a.QuestionRef == dto.Id).ToListAsync();

            question.Text = dto.Text;

            //add new 
            var existedAnswerIds = existedAnswers.Select(x => x.Id).ToList();
            var newItemDtos = dto.Answers
               .Where(x => !existedAnswerIds.Contains(x.Id));

            var newItems = newItemDtos.
                Select(a => new Answer() {
                    Id = a.Id,
                    IsCorrect = a.IsCorrect,
                    OrderNum = a.OrderNum,
                    QuestionRef = dto.Id,
                    Text = dto.Text,
                    InsertedAt = DateTime.Now,
                    InsertedBy = "auto"
                });
            existedAnswers.AddRange(newItems);

            //remove deleted items
            var dtoAnswerIds = dto.Answers.Select(a => a.Id).ToList();
            var deletingItems = existedAnswers
                .Where(x => !dtoAnswerIds.Contains(x.Id));

            //think about it 
            //existedAnswers.RemoveRange(deletingItems);
            _context.Answers.RemoveRange(deletingItems);

            // updating existed answers
            foreach(var ans in existedAnswers)
            {
                var updatedDto =
                    dto.Answers.FirstOrDefault(x => x.Id == ans.Id);
                ans.Text = updatedDto.Text;
                ans.OrderNum = updatedDto.OrderNum;
                ans.IsCorrect = updatedDto.IsCorrect;
            }

            await _context.SaveChangesAsync();
            return true;

        }
    }
}
