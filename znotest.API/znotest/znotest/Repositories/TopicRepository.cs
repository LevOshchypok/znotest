﻿using Dapper;
using System.Data.SqlClient;
using znotest.Dtos;

namespace znotest.Repositories
{
    public class TopicRepository : ITopicRepository
    {
        IConfiguration _configuration;
        ILogger<TopicRepository> _logger;

        public TopicRepository(IConfiguration configuration, ILogger<TopicRepository> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public async Task<int> CreateTopic(TopicDto dto)
        {
            using (var conn = new SqlConnection(_configuration.GetValue<string>("ConnectionStrings:DefaultConnection"))) 
            {
                conn.Open();

                string sql = "insert Topic (name, description, insertedAt, insertedBy) ";
                sql += "select @name, @description, getdate(), 'auto' ";
                sql += "select cast(scope_identity() as int) ";

                int id = await conn.QuerySingleAsync<int>(sql, new { name = dto.Name, description = dto.Description });
                return id;
            }
        }

        public async Task<bool> UpdateTopic(TopicDto dto)
        {
            using (var conn = new SqlConnection(_configuration.GetValue<string>("ConnectionStrings:DefaultConnection")))
            {
                conn.Open();

                string sql = "update Topic set name = @name, description = @description ";
                sql += "where id = @id";
                try
                {
                    await conn.ExecuteAsync(sql, new { name = dto.Name, description = dto.Description, id = dto.Id });
                    return true;
                } catch (Exception e)
                {
                    string message = "Update topic error: " + e.Message;
                    _logger.LogError(message);
                    return false;
                }
                
            }
        }

        public async Task DeleteTopic(int topicId)
        {
            using (var conn = new SqlConnection(_configuration.GetValue<string>("ConnectionStrings:DefaultConnection")))
            {
                conn.Open();

                string sql = "delete from Topic where id = @id";

                await conn.ExecuteAsync(sql, new { id = topicId });
            }
        }

        public async Task<IEnumerable<TopicDto>> GetTopics()
        {
            using (var conn = new SqlConnection(_configuration.GetValue<string>("ConnectionStrings:DefaultConnection")))
            {
                conn.Open();

                string sql = "select id, name, description from Topic ";

                var topics = await conn.QueryAsync<TopicDto>(sql);

                return topics;
            }
        }

        
    }
}
