﻿using znotest.Dtos;
using znotest.Dtos.Shared;

namespace znotest.Repositories
{
    public interface IQuestionRepository
    {
        Task<int> CreateQuestion(QuestionDto dto);
        Task<bool> UpdateQuestion(QuestionDto dto);
        Task DeleteQuestion(int id);
        Task<QuestionDto> GetQuestion(int id);
        Task<GeneralOutput<QuestionDto>> GetQuestionList(int topicRef);
    }
}
