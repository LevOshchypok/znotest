﻿using znotest.Dtos;
using znotest.Entities;

namespace znotest.Repositories
{
    public interface ITopicRepository
    {
        Task<IEnumerable<TopicDto>> GetTopics();
        Task<int> CreateTopic(TopicDto dto);
        Task<bool> UpdateTopic(TopicDto topic);
        Task DeleteTopic(int topicId);
    }
}
