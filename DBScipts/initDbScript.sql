--ALTER TABLE AnswerList NOCHECK CONSTRAINT ALL
--ALTER TABLE Question NOCHECK CONSTRAINT ALL
--ALTER TABLE Topic NOCHECK CONSTRAINT ALL
if OBJECT_ID('QuestionOption') is not null drop table QuestionOption
if OBJECT_ID('Question') is not null drop table Question
if OBJECT_ID('Topic') is not null drop table Topic



go 


create table Topic
(id int identity(1,1) constraint PK_Topic primary key, 
 name nvarchar(200), 
 description nvarchar(200), 
 insertedBy nvarchar(200), 
 insertedAt datetime
)


create table Question
(
id int identity(1,1) constraint PK_Question primary key, 
text nvarchar(max), 
topicRef int,
insertedBy nvarchar(200), 
insertedAt datetime,
constraint FK_Question_TopicRef foreign key (topicRef) 
references Topic (id) 
)

create table Answer
(
id int identity(1,1) constraint PK_AnswerList primary key,
text nvarchar(max), 
orderNum int, 
isCorrect bit constraint DF_AnswerIsCorrect default (0), 
questionRef int, 
insertedBy nvarchar(200), 
insertedAt datetime,
constraint FK_QuestionOption_Question foreign key (questionRef) 
references Question (id) 
)
